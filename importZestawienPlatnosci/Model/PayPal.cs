﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class PayPal
    {
        public decimal idRachunku { get; set; }
        public string nrKonta { get; set; }
        public decimal idRozrachunku { get; set; }
        public string STATUS_POZYCJI { get; set; }

        public DateTime Data { get; set; }
        public string Godzina { get; set; }
        public string Strefa_czasowa { get; set; }
        public string Opis { get; set; }
        public string Waluta { get; set; }
        public decimal Brutto { get; set; }
        public decimal Oplata { get; set; }
        public decimal Netto { get; set; }
        public decimal Saldo { get; set; }
        public string Numer_transakcji { get; set; }
        public string Z_adresu_email { get; set; }
        public string Nazwa { get; set; }
        public string Nazwa_banku { get; set; }
        public string Rachunek_bankowy { get; set; }
        public decimal Koszt_wysylki_oraz_koszty_manipulacyjne { get; set; }
        public decimal Kwota_podatku { get; set; }
        public string Identyfikator_faktury { get; set; }
        public string Pomocniczy_numer_transakcji { get; set; }
    }
}
