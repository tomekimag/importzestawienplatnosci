﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class Schenker
    {
        public decimal idRachunku { get; set; }
        public string nrKonta { get; set; }
        public decimal idRozrachunku { get; set; }
        public string STATUS_POZYCJI { get; set; }

        public string Odbiorca { get; set; }
        public string Numer_listu { get; set; }
        public string Opis { get; set; }
        public decimal Kwota { get; set; }
        public DateTime Data_przelewu { get; set; }
    }
}
