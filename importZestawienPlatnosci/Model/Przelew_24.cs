﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class Przelew_24
    {
        public decimal idRachunku { get; set; }
        public string nrKonta { get; set; }
        public decimal idRozrachunku { get; set; }
        public string STATUS_POZYCJI { get; set; }
        public int Lp { get; set; }
        public string ID_transakcji { get; set; }
        public string Data_przyjecia_ { get; set; }
        public string Data_wplaty_ { get; set; }
        public DateTime Data_przyjecia { get; set; }
        public DateTime Data_wplaty { get; set; }
        public int Kwota_gr { get; set; }
        public string Klient { get; set; }
        public string Kraj { get; set; }
        public string Miasto { get; set; }
        public string Kod_pocztowy { get; set; }
        public string Adres { get; set; }
        public string E_mail { get; set; }
        public string Wynik_transakcji { get; set; }
        public string Nr_paczki { get; set; }
        public string Forma_platnosci { get; set; }
        public string Tytul { get; set; }
        public string ID_sesji { get; set; }
    }
}
