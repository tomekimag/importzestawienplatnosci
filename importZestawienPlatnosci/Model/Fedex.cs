﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class Fedex
    {
        public decimal idRachunku { get; set; }
        public string nrKonta { get; set; }
        public decimal idRozrachunku { get; set; }
        public string STATUS_POZYCJI { get; set; }
        public DateTime Data_przyjecia { get; set; }
        public DateTime Data_wplaty { get; set; }

        public string Data_przekazania_do_banku { get; set; }//Data przekazania do banku
        public string Nr_listu { get; set; }//Nr listu
        public string Data_nadania { get; set; }//Data nadania
        public string Opis_zawartosci { get; set; }//Opis zawartości
        public string Uwagi { get; set; }//Uwagi
        public string Numer_zewnetrzny { get; set; }//Numer zewnętrzny
        public string Odbiorca { get; set; }//Odbiorca 
        public decimal Kwota { get; set; }//Kwota
    }
}
