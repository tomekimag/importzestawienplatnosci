﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class RachunekFirmyModel
    {
        public decimal ID_RACHUNKU { get; set; }
        public string SYM_WALUTY { get; set; }
        public string NUMER_RACHUNKU { get; set; }
        public string LK { get; set; }
        public Nullable<byte> AKTYWNY { get; set; }
        public Nullable<int> KOD_RACHUNKU { get; set; }
        public string NAZWA { get; set; }
        public string KONTO_FK { get; set; }
    }
}
