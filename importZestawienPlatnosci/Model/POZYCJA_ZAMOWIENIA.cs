//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace importZestawienPlatnosci.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class POZYCJA_ZAMOWIENIA
    {
        public decimal ID_POZYCJI_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_ARTYKULU { get; set; }
        public string KOD_VAT { get; set; }
        public decimal ZAMOWIONO { get; set; }
        public decimal ZREALIZOWANO { get; set; }
        public decimal DO_REALIZACJI { get; set; }
        public decimal ZAREZERWOWANO { get; set; }
        public decimal DO_REZERWACJI { get; set; }
        public decimal CENA_NETTO { get; set; }
        public decimal CENA_BRUTTO { get; set; }
        public Nullable<decimal> CENA_NETTO_WAL { get; set; }
        public Nullable<decimal> CENA_BRUTTO_WAL { get; set; }
        public decimal PRZELICZNIK { get; set; }
        public string JEDNOSTKA { get; set; }
        public decimal NARZUT { get; set; }
        public decimal DO_REZ_USER { get; set; }
        public Nullable<decimal> DO_REZ_POP { get; set; }
        public decimal STAN_ZREALIZOWANO { get; set; }
        public Nullable<byte> TYP { get; set; }
        public string OPIS { get; set; }
        public Nullable<byte> TRYBREJESTRACJI { get; set; }
        public Nullable<decimal> ID_DOSTAWY_REZ { get; set; }
        public string POLE1 { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE4 { get; set; }
        public string POLE5 { get; set; }
        public string POLE6 { get; set; }
        public string POLE7 { get; set; }
        public string POLE8 { get; set; }
        public string POLE9 { get; set; }
        public string POLE10 { get; set; }
        public Nullable<decimal> ID_WARIANTU { get; set; }
        public string ZNACZNIK_CENY { get; set; }
        public Nullable<decimal> ID_POZYCJI_OFERTY { get; set; }
        public string NR_SERII { get; set; }
    }
}
