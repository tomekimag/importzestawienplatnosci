﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importZestawienPlatnosci.Model
{
    public class RozrachunekModel
    {
        public decimal ID_ROZRACHUNKU { get; set; }

        /// <summary>
        /// Z tabeli dokument_handlowy.
        /// </summary>
        public string KONTRAHENT_NAZWA { get; set; }

        public string TYP_DOK { get; set; }
        public Nullable<decimal> KWOTA { get; set; }
        public Nullable<decimal> KWOTA_W { get; set; }
        public Nullable<decimal> POZOSTALO { get; set; }
        public Nullable<decimal> POZOSTALO_W { get; set; }
        public Nullable<decimal> POZOSTALO_K { get; set; }
        public Nullable<decimal> POZOSTALO_W_K { get; set; }
        public string SYM_WALUTY { get; set; }
        public string STRONA { get; set; }
        public string SYMBOL { get; set; }
        public string NR_DOK { get; set; }
        public Nullable<int> DATA_DOK { get; set; }
        public DateTime DATA_DOK_ { get; set; }
        public Nullable<int> TERMIN_PLATNOSCI { get; set; }
        public string RODZAJ { get; set; }
        public string OPIS { get; set; }
        public string UWAGI { get; set; }
        public string FORMA_PLATNOSCI { get; set; }
        public string NR_KONTA { get; set; }
        public string NAZWA_KONTA { get; set; }
        public Nullable<int> KIERUNEK_ZAPLATY { get; set; }
    }
}
