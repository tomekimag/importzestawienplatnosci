﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importZestawienPlatnosci.Model;
using importZestawienPlatnosci.Controllers;

namespace importZestawienPlatnosci
{
    public partial class Form_rozrachunki : Form
    {
        private List<RozrachunekModel> rozrachunki;

        public decimal idRozrachunku = 0;

        public Form_rozrachunki()
        {
            InitializeComponent();
            ZaladujRozrachunkiDoSiatki();
        }

        private void ZaladujRozrachunkiDoSiatki()
        {
            dataGridView_rozrachunki.DataSource = rozrachunki = RozrachunekController.PobierzNierozliczoneRozrachunki();
        }

        private void dataGridView_rozrachunki_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ID_ROZRACHUNKU":
                    e.Column.Visible = false;
                    break;
                case "DATA_DOK":
                    e.Column.Visible = false;
                    break;
                case "Data_wplaty_":
                    e.Column.Visible = false;
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_rozrachunki_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            //dgv.AutoResizeColumns();
            //dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_rozrachunki.SelectedRows == null)
            {
                MessageBox.Show("Wskarz rozrachunek do rozliczenia.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_rozrachunki.SelectedRows.Count != 0)
            {
                idRozrachunku = decimal.Parse(dataGridView_rozrachunki["ID_ROZRACHUNKU", dataGridView_rozrachunki.SelectedRows[0].Index].Value.ToString());

                if (idRozrachunku != 0)
                {
                    this.DialogResult = DialogResult.Yes;
                    Close();
                }
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            if (textBox_ciagSzukany.Text == string.Empty) { MessageBox.Show("Wprowadź szukany ciąg nazwy.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return;  }

            dataGridView_rozrachunki.DataSource = rozrachunki.Where(x => x.KONTRAHENT_NAZWA.ToUpper().Contains(textBox_ciagSzukany.Text.ToUpper())).ToList();
        }

        private void button_pokazWszystko_Click(object sender, EventArgs e)
        {
            dataGridView_rozrachunki.DataSource = rozrachunki;
        }
    }
}
