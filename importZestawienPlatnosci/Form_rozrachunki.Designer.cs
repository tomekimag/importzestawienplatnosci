﻿namespace importZestawienPlatnosci
{
    partial class Form_rozrachunki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_rozrachunki = new System.Windows.Forms.DataGridView();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_ciagSzukany = new System.Windows.Forms.TextBox();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.button_pokazWszystko = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozrachunki)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wybierz);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 455);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(990, 64);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_pokazWszystko);
            this.panel2.Controls.Add(this.button_szukaj);
            this.panel2.Controls.Add(this.textBox_ciagSzukany);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(990, 63);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_rozrachunki);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(990, 392);
            this.panel3.TabIndex = 2;
            // 
            // dataGridView_rozrachunki
            // 
            this.dataGridView_rozrachunki.AllowUserToAddRows = false;
            this.dataGridView_rozrachunki.AllowUserToDeleteRows = false;
            this.dataGridView_rozrachunki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_rozrachunki.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_rozrachunki.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_rozrachunki.Name = "dataGridView_rozrachunki";
            this.dataGridView_rozrachunki.ReadOnly = true;
            this.dataGridView_rozrachunki.Size = new System.Drawing.Size(990, 392);
            this.dataGridView_rozrachunki.TabIndex = 0;
            this.dataGridView_rozrachunki.DataSourceChanged += new System.EventHandler(this.dataGridView_rozrachunki_DataSourceChanged);
            this.dataGridView_rozrachunki.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_rozrachunki_ColumnAdded);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(887, 19);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(806, 19);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 1;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Szukanie po nazwie kontrahenta:";
            // 
            // textBox_ciagSzukany
            // 
            this.textBox_ciagSzukany.Location = new System.Drawing.Point(15, 25);
            this.textBox_ciagSzukany.Name = "textBox_ciagSzukany";
            this.textBox_ciagSzukany.Size = new System.Drawing.Size(295, 20);
            this.textBox_ciagSzukany.TabIndex = 1;
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(316, 23);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 23);
            this.button_szukaj.TabIndex = 2;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // button_pokazWszystko
            // 
            this.button_pokazWszystko.Location = new System.Drawing.Point(397, 23);
            this.button_pokazWszystko.Name = "button_pokazWszystko";
            this.button_pokazWszystko.Size = new System.Drawing.Size(75, 23);
            this.button_pokazWszystko.TabIndex = 3;
            this.button_pokazWszystko.Text = "Wszystko";
            this.button_pokazWszystko.UseVisualStyleBackColor = true;
            this.button_pokazWszystko.Click += new System.EventHandler(this.button_pokazWszystko_Click);
            // 
            // Form_rozrachunki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 519);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_rozrachunki";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Rozrachunki";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozrachunki)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView_rozrachunki;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.TextBox textBox_ciagSzukany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_pokazWszystko;
        private System.Windows.Forms.Button button_szukaj;
    }
}