﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importZestawienPlatnosci.Model;
using importZestawienPlatnosci.Controllers;

namespace importZestawienPlatnosci
{
    public partial class Form1 : Form
    {
        private string sciezkaDoPliku;

        private List<Przelew_24> przelewy_24;
        private List<Fedex> przelewy_fedex;
        private List<PayPal> przelewy_payPal;
        private List<Schenker> przelewy_schenker;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_wczytaj_Click(object sender, EventArgs e)
        {
            if (comboBox_rodzajPliku.Text == string.Empty) { MessageBox.Show("Wyberz rodzaj pliku.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

            comboBox_rodzajPliku.Enabled = false;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sciezkaDoPliku = openFileDialog1.FileName;
                //MessageBox.Show("Check 1 - przed wczytaniem pliku");
                WczytajPlikExcel();
                //MessageBox.Show("Check 1 - po wczytaniu pliku - przed rozpisaniem platnosci");
                RozpoznajPlatnosci();
                //MessageBox.Show("Check 1 - po rozpisaniu platnosci");
                WyswietlPrzelewyWSiatce();
            }
        }

        private void WczytajPlikExcel()
        {
            try
            {
/*Przelewy 24
Schenker
PayPal
Fedex
Raben*/
                if (comboBox_rodzajPliku.Text == "Przelewy 24")
                {
                    przelewy_24 = Przelewy_24Controller.pobierzPrzelewy(sciezkaDoPliku);
                }
                else if(comboBox_rodzajPliku.Text == "Fedex")
                {
                    przelewy_fedex = FedexController.pobierzPrzelewy(sciezkaDoPliku);
                }
                else if (comboBox_rodzajPliku.Text == "PayPal")
                {
                    przelewy_payPal = PayPalController.pobierzPrzelewy(sciezkaDoPliku);
                }
                else if (comboBox_rodzajPliku.Text == "Schenker")
                {
                    przelewy_schenker = SchenkerController.pobierzPrzelewy(sciezkaDoPliku);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($@"Błąd czytania pliku: {e.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void RozpoznajPlatnosci()
        {
            if (comboBox_rodzajPliku.Text == "Przelewy 24")
            {
                foreach (Przelew_24 przelew in przelewy_24)
                {
                    przelew.idRozrachunku = RozrachunekController.PobierzIdRozrachunku(((decimal)(przelew.Kwota_gr) / 100), przelew.E_mail);

                    if (przelew.idRozrachunku != 0) przelew.STATUS_POZYCJI = "Ok";
                }
            }
            else if (comboBox_rodzajPliku.Text == "Fedex")
            {
                foreach (Fedex przelew in przelewy_fedex)
                {
                    przelew.idRozrachunku = RozrachunekController.PobierzIdRozrachunkuFedex(przelew.Nr_listu.Substring(2, przelew.Nr_listu.Length - 2));
                    if (przelew.idRozrachunku != 0) przelew.STATUS_POZYCJI = "Ok";
                }
            }
            else if (comboBox_rodzajPliku.Text == "PayPal")
            {
                foreach (PayPal przelew in przelewy_payPal)
                {
                    przelew.idRozrachunku = RozrachunekController.PobierzIdRozrachunkuPayPal(przelew);
                    if (przelew.idRozrachunku != 0) przelew.STATUS_POZYCJI = "Ok";
                }
            }
            else if (comboBox_rodzajPliku.Text == "Schenker")
            {
                /*
                foreach (Schenker przelew in przelewy_schenker)
                {
                    przelew.idRozrachunku = RozrachunekController.PobierzIdRozrachunkuSchenker(przelew);
                    if (przelew.idRozrachunku != 0) przelew.STATUS_POZYCJI = "Ok";
                }*/
            }
        }

        private void WyswietlPrzelewyWSiatce()
        {
            if (comboBox_rodzajPliku.Text == "Przelewy 24")
            {
                dataGridView_przelewy.DataSource = przelewy_24;
            }
            else if (comboBox_rodzajPliku.Text == "Fedex")
            {
                dataGridView_przelewy.DataSource = przelewy_fedex;
            }
            else if (comboBox_rodzajPliku.Text == "PayPal")
            {
                dataGridView_przelewy.DataSource = przelewy_payPal;
            }
            else if (comboBox_rodzajPliku.Text == "Schenker")
            {
                dataGridView_przelewy.DataSource = przelewy_schenker;
            }
        }

        private void dataGridView_przelewy_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "idRachunku":
                    e.Column.Visible = false;
                    break;
                case "idRozrachunku":
                    e.Column.Visible = false;
                    break;
                case "Data_przyjecia_":
                    e.Column.Visible = false;
                    break;
                case "Data_wplaty_":
                    e.Column.Visible = false;
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_przelewy_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            //dgv.AutoResizeColumns();
            //dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_powiazZRozrachunkiem_Click(object sender, EventArgs e)
        {
            if (dataGridView_przelewy.SelectedRows == null)
            {
                MessageBox.Show("Wskarz przelew.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_przelewy.SelectedRows.Count != 0)
            {
                if (comboBox_rodzajPliku.Text == "Przelewy 24")
                {
                    string statusPozycji = dataGridView_przelewy["STATUS_POZYCJI", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    string email = dataGridView_przelewy["E_mail", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    int kwota_gr = int.Parse(dataGridView_przelewy["Kwota_gr", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString());

                    if (statusPozycji == "Ok") { MessageBox.Show("Przelew jest powiązany z rozrachunkiem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_rozrachunki rozrachunki = new Form_rozrachunki();
                    if (rozrachunki.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Przelew_24 przelew in przelewy_24)
                        {
                            if ((przelew.Kwota_gr == kwota_gr) && (przelew.E_mail == email))
                            {
                                przelew.idRozrachunku = rozrachunki.idRozrachunku;
                                przelew.STATUS_POZYCJI = "Ok";
                            }
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_24;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "Fedex")
                {
                    string statusPozycji = dataGridView_przelewy["STATUS_POZYCJI", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    string nrListu = dataGridView_przelewy["Nr_listu", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();

                    if (statusPozycji == "Ok") { MessageBox.Show("Przelew jest powiązany z rozrachunkiem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_rozrachunki rozrachunki = new Form_rozrachunki();
                    if (rozrachunki.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Fedex przelew in przelewy_fedex)
                        {
                            if (przelew.Nr_listu == nrListu)
                            {
                                przelew.idRozrachunku = rozrachunki.idRozrachunku;
                                przelew.STATUS_POZYCJI = "Ok";
                            }
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_fedex;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "PayPal")
                {
                    string statusPozycji = dataGridView_przelewy["STATUS_POZYCJI", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    string numerTransakcji = dataGridView_przelewy["Numer_transakcji", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();

                    if (statusPozycji == "Ok") { MessageBox.Show("Przelew jest powiązany z rozrachunkiem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_rozrachunki rozrachunki = new Form_rozrachunki();
                    if (rozrachunki.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (PayPal przelew in przelewy_payPal)
                        {
                            if (przelew.Numer_transakcji == numerTransakcji)
                            {
                                przelew.idRozrachunku = rozrachunki.idRozrachunku;
                                przelew.STATUS_POZYCJI = "Ok";
                            }
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_payPal;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "Schenker")
                {
                    string statusPozycji = dataGridView_przelewy["STATUS_POZYCJI", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    string numerListu = dataGridView_przelewy["Numer_listu", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();

                    if (statusPozycji == "Ok") { MessageBox.Show("Przelew jest powiązany z rozrachunkiem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_rozrachunki rozrachunki = new Form_rozrachunki();
                    if (rozrachunki.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Schenker przelew in przelewy_schenker)
                        {
                            if (przelew.Numer_listu == numerListu)
                            {
                                przelew.idRozrachunku = rozrachunki.idRozrachunku;
                                przelew.STATUS_POZYCJI = "Ok";
                            }
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_schenker;
                    }
                }
            }
        }

        private void comboBox_rodzajPliku_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox_rodzajPliku.Text == "Przelewy 24")
                MessageBox.Show("Plik >>przelewy 24<< przed zaimportowaniem należy otworzyć w excelu i zapisać jako xls.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (comboBox_rodzajPliku.Text == "Fedex")
                MessageBox.Show("Plik >>Fedex<< przed zaimportowaniem należy otworzyć w excelu i zapisać jako xls.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else if (comboBox_rodzajPliku.Text == "Schenker")
                MessageBox.Show("Plik >>Schenker<< przed zaimportowaniem należy otworzyć w excelu i zapisać jako csv (rozdzielany przecinkami).", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_zaimportuj_Click(object sender, EventArgs e)
        {
            if ((comboBox_rodzajPliku.Text == "Przelewy 24") && (przelewy_24 != null))
            {
                Przelew_24 przelew = przelewy_24.Where(x => x.STATUS_POZYCJI != "Ok" || x.idRachunku == 0).FirstOrDefault();

                if (przelew != null) { MessageBox.Show("Wszystkie płatności muszą zostać rozpoznane oraz konta do nich muszą być przypisane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                przelewy_24.ForEach(DokumentFinansowyController.UtworzDokumentFinansowy);

                MessageBox.Show("Dokumenty zostały zaimportowane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dataGridView_przelewy.DataSource = przelewy_24 = null;
            }
            else if ((comboBox_rodzajPliku.Text == "Fedex") && (przelewy_fedex != null))
            {
                Fedex przelew = przelewy_fedex.Where(x => x.STATUS_POZYCJI != "Ok" || x.idRachunku == 0).FirstOrDefault();

                if (przelew != null) { MessageBox.Show("Wszystkie płatności muszą zostać rozpoznane oraz konta do nich muszą być przypisane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                przelewy_fedex.ForEach(DokumentFinansowyController.UtworzDokumentFinansowy);

                MessageBox.Show("Dokumenty zostały zaimportowane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dataGridView_przelewy.DataSource = przelewy_24 = null;
            }
            else if ((comboBox_rodzajPliku.Text == "PayPal") && (przelewy_payPal != null))
            {
                PayPal przelew = przelewy_payPal.Where(x => x.STATUS_POZYCJI != "Ok" || x.idRachunku == 0).FirstOrDefault();

                if (przelew != null) { MessageBox.Show("Wszystkie płatności muszą zostać rozpoznane oraz konta do nich muszą być przypisane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                przelewy_payPal.ForEach(DokumentFinansowyController.UtworzDokumentFinansowy);

                MessageBox.Show("Dokumenty zostały zaimportowane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dataGridView_przelewy.DataSource = przelewy_payPal = null;
            }
            else if ((comboBox_rodzajPliku.Text == "Schenker") && (przelewy_schenker != null))
            {
                Schenker przelew = przelewy_schenker.Where(x => x.STATUS_POZYCJI != "Ok" || x.idRachunku == 0).FirstOrDefault();

                if (przelew != null) { MessageBox.Show("Wszystkie płatności muszą zostać rozpoznane oraz konta do nich muszą być przypisane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                przelewy_schenker.ForEach(DokumentFinansowyController.UtworzDokumentFinansowy);

                MessageBox.Show("Dokumenty zostały zaimportowane.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dataGridView_przelewy.DataSource = przelewy_payPal = null;
            }
            comboBox_rodzajPliku.Enabled = true; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView_przelewy.SelectedRows == null)
            {
                MessageBox.Show("Wskarz przelew.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_przelewy.SelectedRows.Count != 0)
            {
                if (comboBox_rodzajPliku.Text == "Przelewy 24")
                {
                    string forma_platnosci = dataGridView_przelewy["Forma_platnosci", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString();
                    decimal idRachunku = decimal.Parse(dataGridView_przelewy["idRachunku", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString());

                    if (idRachunku != 0) { MessageBox.Show("Przelew jest powiązany z kontem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_kontaBankowe konta = new Form_kontaBankowe();
                    if (konta.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Przelew_24 przelew in przelewy_24)
                        {
                            if (przelew.Forma_platnosci == forma_platnosci)
                            {
                                przelew.idRachunku = konta.idRachunku;
                                przelew.nrKonta = konta.nrKonta;
                            }
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_24;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "Fedex")
                {
                    decimal idRachunku = decimal.Parse(dataGridView_przelewy["idRachunku", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString());

                    if (idRachunku != 0) { MessageBox.Show("Przelew jest powiązany z kontem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_kontaBankowe konta = new Form_kontaBankowe();
                    if (konta.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Fedex przelew in przelewy_fedex)
                        {
                            przelew.idRachunku = konta.idRachunku;
                            przelew.nrKonta = konta.nrKonta;
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_fedex;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "PayPal")
                {
                    decimal idRachunku = decimal.Parse(dataGridView_przelewy["idRachunku", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString());

                    if (idRachunku != 0) { MessageBox.Show("Przelew jest powiązany z kontem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_kontaBankowe konta = new Form_kontaBankowe();
                    if (konta.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (PayPal przelew in przelewy_payPal)
                        {
                            przelew.idRachunku = konta.idRachunku;
                            przelew.nrKonta = konta.nrKonta;
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_payPal;
                    }
                }
                else if (comboBox_rodzajPliku.Text == "Schenker")
                {
                    decimal idRachunku = decimal.Parse(dataGridView_przelewy["idRachunku", dataGridView_przelewy.SelectedRows[0].Index].Value.ToString());

                    if (idRachunku != 0) { MessageBox.Show("Przelew jest powiązany z kontem.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

                    Form_kontaBankowe konta = new Form_kontaBankowe();
                    if (konta.ShowDialog() == DialogResult.Yes)
                    {
                        foreach (Schenker przelew in przelewy_schenker)
                        {
                            przelew.idRachunku = konta.idRachunku;
                            przelew.nrKonta = konta.nrKonta;
                        }

                        dataGridView_przelewy.DataSource = null;
                        dataGridView_przelewy.DataSource = przelewy_schenker;
                    }
                }
            }
        }
    }
}
