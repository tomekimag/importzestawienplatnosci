﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importZestawienPlatnosci.Controllers;
using importZestawienPlatnosci.Model;

namespace importZestawienPlatnosci
{
    public partial class Form_kontaBankowe : Form
    {
        public decimal idRachunku = 0;
        public string nrKonta = string.Empty;

        public Form_kontaBankowe()
        {
            InitializeComponent();
            ZaladujKontaBankoweDoSiatki();
        }

        private void ZaladujKontaBankoweDoSiatki()
        {
            dataGridView_kontaBankowe.DataSource = RachunekFirmyController.PobierzRachunkiFirmy();
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_kontaBankowe.SelectedRows == null)
            {
                MessageBox.Show("Wskarz przelew.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_kontaBankowe.SelectedRows.Count != 0)
            {
                nrKonta = dataGridView_kontaBankowe["NUMER_RACHUNKU", dataGridView_kontaBankowe.SelectedRows[0].Index].Value.ToString();
                idRachunku = decimal.Parse(dataGridView_kontaBankowe["ID_RACHUNKU", dataGridView_kontaBankowe.SelectedRows[0].Index].Value.ToString());

                if (idRachunku != 0)
                {
                    this.DialogResult = DialogResult.Yes;
                    Close();
                }
            }
        }

        private void dataGridView_kontaBankowe_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
    }
}
