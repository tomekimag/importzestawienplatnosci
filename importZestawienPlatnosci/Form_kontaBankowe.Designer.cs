﻿namespace importZestawienPlatnosci
{
    partial class Form_kontaBankowe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView_kontaBankowe = new System.Windows.Forms.DataGridView();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_kontaBankowe)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wybierz);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 419);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1056, 71);
            this.panel1.TabIndex = 0;
            // 
            // dataGridView_kontaBankowe
            // 
            this.dataGridView_kontaBankowe.AllowUserToAddRows = false;
            this.dataGridView_kontaBankowe.AllowUserToDeleteRows = false;
            this.dataGridView_kontaBankowe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_kontaBankowe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_kontaBankowe.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_kontaBankowe.Name = "dataGridView_kontaBankowe";
            this.dataGridView_kontaBankowe.ReadOnly = true;
            this.dataGridView_kontaBankowe.Size = new System.Drawing.Size(1056, 419);
            this.dataGridView_kontaBankowe.TabIndex = 1;
            this.dataGridView_kontaBankowe.DataSourceChanged += new System.EventHandler(this.dataGridView_kontaBankowe_DataSourceChanged);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(949, 24);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 3;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(868, 24);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 4;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // Form_kontaBankowe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 490);
            this.Controls.Add(this.dataGridView_kontaBankowe);
            this.Controls.Add(this.panel1);
            this.Name = "Form_kontaBankowe";
            this.Text = "Konta bankowe";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_kontaBankowe)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView_kontaBankowe;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Button button_anuluj;
    }
}