﻿namespace importZestawienPlatnosci
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_wczytaj = new System.Windows.Forms.Button();
            this.comboBox_rodzajPliku = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_przelewy = new System.Windows.Forms.DataGridView();
            this.richTextBox_komunikat = new System.Windows.Forms.RichTextBox();
            this.button_powiazZRozrachunkiem = new System.Windows.Forms.Button();
            this.button_zaimportuj = new System.Windows.Forms.Button();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_przelewy)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Controls.Add(this.button_zaimportuj);
            this.panel1.Controls.Add(this.button_powiazZRozrachunkiem);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 435);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1007, 80);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.richTextBox_komunikat);
            this.panel2.Controls.Add(this.button_wczytaj);
            this.panel2.Controls.Add(this.comboBox_rodzajPliku);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1007, 63);
            this.panel2.TabIndex = 1;
            // 
            // button_wczytaj
            // 
            this.button_wczytaj.Location = new System.Drawing.Point(263, 23);
            this.button_wczytaj.Name = "button_wczytaj";
            this.button_wczytaj.Size = new System.Drawing.Size(87, 23);
            this.button_wczytaj.TabIndex = 2;
            this.button_wczytaj.Text = "Wczytaj";
            this.button_wczytaj.UseVisualStyleBackColor = true;
            this.button_wczytaj.Click += new System.EventHandler(this.button_wczytaj_Click);
            // 
            // comboBox_rodzajPliku
            // 
            this.comboBox_rodzajPliku.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_rodzajPliku.FormattingEnabled = true;
            this.comboBox_rodzajPliku.Items.AddRange(new object[] {
            "Przelewy 24",
            "Schenker",
            "PayPal",
            "Fedex",
            "Raben"});
            this.comboBox_rodzajPliku.Location = new System.Drawing.Point(15, 25);
            this.comboBox_rodzajPliku.Name = "comboBox_rodzajPliku";
            this.comboBox_rodzajPliku.Size = new System.Drawing.Size(242, 21);
            this.comboBox_rodzajPliku.TabIndex = 1;
            this.comboBox_rodzajPliku.SelectedValueChanged += new System.EventHandler(this.comboBox_rodzajPliku_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rodzaj pliku:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_przelewy);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1007, 372);
            this.panel3.TabIndex = 2;
            // 
            // dataGridView_przelewy
            // 
            this.dataGridView_przelewy.AllowUserToAddRows = false;
            this.dataGridView_przelewy.AllowUserToDeleteRows = false;
            this.dataGridView_przelewy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_przelewy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_przelewy.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_przelewy.Name = "dataGridView_przelewy";
            this.dataGridView_przelewy.ReadOnly = true;
            this.dataGridView_przelewy.Size = new System.Drawing.Size(1007, 372);
            this.dataGridView_przelewy.TabIndex = 0;
            this.dataGridView_przelewy.DataSourceChanged += new System.EventHandler(this.dataGridView_przelewy_DataSourceChanged);
            this.dataGridView_przelewy.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_przelewy_ColumnAdded);
            // 
            // richTextBox_komunikat
            // 
            this.richTextBox_komunikat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_komunikat.Enabled = false;
            this.richTextBox_komunikat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox_komunikat.Location = new System.Drawing.Point(455, 4);
            this.richTextBox_komunikat.Name = "richTextBox_komunikat";
            this.richTextBox_komunikat.Size = new System.Drawing.Size(548, 54);
            this.richTextBox_komunikat.TabIndex = 3;
            this.richTextBox_komunikat.Text = "Uwaga!!! Nagłówki kolum w plikach nie mogą zawerać znaków specjalnych.";
            // 
            // button_powiazZRozrachunkiem
            // 
            this.button_powiazZRozrachunkiem.Location = new System.Drawing.Point(188, 26);
            this.button_powiazZRozrachunkiem.Name = "button_powiazZRozrachunkiem";
            this.button_powiazZRozrachunkiem.Size = new System.Drawing.Size(151, 23);
            this.button_powiazZRozrachunkiem.TabIndex = 0;
            this.button_powiazZRozrachunkiem.Text = "Powiąż z rozrachunkiem";
            this.button_powiazZRozrachunkiem.UseVisualStyleBackColor = true;
            this.button_powiazZRozrachunkiem.Click += new System.EventHandler(this.button_powiazZRozrachunkiem_Click);
            // 
            // button_zaimportuj
            // 
            this.button_zaimportuj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zaimportuj.Location = new System.Drawing.Point(823, 26);
            this.button_zaimportuj.Name = "button_zaimportuj";
            this.button_zaimportuj.Size = new System.Drawing.Size(75, 23);
            this.button_zaimportuj.TabIndex = 1;
            this.button_zaimportuj.Text = "Importuj";
            this.button_zaimportuj.UseVisualStyleBackColor = true;
            this.button_zaimportuj.Click += new System.EventHandler(this.button_zaimportuj_Click);
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(904, 26);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 2;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Powiąż konto bankowe";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 515);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Import płatności";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_przelewy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button_wczytaj;
        private System.Windows.Forms.ComboBox comboBox_rodzajPliku;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_przelewy;
        private System.Windows.Forms.RichTextBox richTextBox_komunikat;
        private System.Windows.Forms.Button button_powiazZRozrachunkiem;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button_zaimportuj;
        private System.Windows.Forms.Button button1;
    }
}

