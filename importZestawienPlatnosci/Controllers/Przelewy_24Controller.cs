﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;
using LinqToExcel;

namespace importZestawienPlatnosci.Controllers
{
    public static class Przelewy_24Controller
    {
        public static List<Przelew_24> pobierzPrzelewy(string sciezkaDoPliku)
        {
            List<Przelew_24> przelewy_24;

            var excel = new ExcelQueryFactory(sciezkaDoPliku);

            
            excel.AddMapping<Przelew_24>(x => x.Lp, "Lp");
            excel.AddMapping<Przelew_24>(x => x.ID_transakcji, "ID transakcji");
            excel.AddMapping<Przelew_24>(x => x.Data_przyjecia_, "Data przyjÄ™cia");
            excel.AddMapping<Przelew_24>(x => x.Data_wplaty_, "Data wpĹ‚aty");
            excel.AddMapping<Przelew_24>(x => x.Kwota_gr, "Kwota_gr");
            excel.AddMapping<Przelew_24>(x => x.Klient, "Klient");
            excel.AddMapping<Przelew_24>(x => x.Kraj, "Kraj");
            excel.AddMapping<Przelew_24>(x => x.Miasto, "Miasto");
            excel.AddMapping<Przelew_24>(x => x.Kod_pocztowy, "Kod pocztowy");
            excel.AddMapping<Przelew_24>(x => x.Adres, "Adres");
            excel.AddMapping<Przelew_24>(x => x.E_mail, "E-mail");
            excel.AddMapping<Przelew_24>(x => x.Wynik_transakcji, "Wynik transakcji");
            excel.AddMapping<Przelew_24>(x => x.Nr_paczki, "Nr paczki");
            excel.AddMapping<Przelew_24>(x => x.Forma_platnosci, "Forma pĹ‚atnoĹ›ci");
            excel.AddMapping<Przelew_24>(x => x.Tytul, "TytuĹ‚");
            excel.AddMapping<Przelew_24>(x => x.ID_sesji, "ID sesji");
            
            /*
            excel.AddMapping<Przelew_24>(x => x.Lp, "Lp");
            excel.AddMapping<Przelew_24>(x => x.ID_transakcji, "ID transakcji");
            excel.AddMapping<Przelew_24>(x => x.Data_przyjecia_, "Data przyjęcia");
            excel.AddMapping<Przelew_24>(x => x.Data_wplaty_, "Data wpłaty");
            excel.AddMapping<Przelew_24>(x => x.Kwota_gr, "Kwota_gr");
            excel.AddMapping<Przelew_24>(x => x.Klient, "Klient");
            excel.AddMapping<Przelew_24>(x => x.Kraj, "Kraj");
            excel.AddMapping<Przelew_24>(x => x.Miasto, "Miasto");
            excel.AddMapping<Przelew_24>(x => x.Kod_pocztowy, "Kod pocztowy");
            excel.AddMapping<Przelew_24>(x => x.Adres, "Adres");
            excel.AddMapping<Przelew_24>(x => x.E_mail, "E-mail");
            excel.AddMapping<Przelew_24>(x => x.Wynik_transakcji, "Wynik transakcji");
            excel.AddMapping<Przelew_24>(x => x.Nr_paczki, "Nr paczki");
            excel.AddMapping<Przelew_24>(x => x.Forma_platnosci, "Forma płatności");
            excel.AddMapping<Przelew_24>(x => x.Tytul, "Tytuł");
            excel.AddMapping<Przelew_24>(x => x.ID_sesji, "ID sesji");
            */
            //przelewy_24 = (from c in excel.Worksheet<Przelew_24>("zestawprzelewy24")
            //         where c.Klient != ""
            //       select c).ToList();
            przelewy_24 = (from c in excel.WorksheetRange<Przelew_24>("A4", "P10000", "zestawprzelewy24")
                           where c.Klient != ""
                           select c).ToList();
            foreach (Przelew_24 pozycja in przelewy_24)
            {
                pozycja.STATUS_POZYCJI = "Nie rozpoznany";
                pozycja.Data_przyjecia = DateTime.Parse(pozycja.Data_przyjecia_);
                pozycja.Data_wplaty = DateTime.Parse(pozycja.Data_wplaty_);
            }

            return przelewy_24;
        }
    }
}
