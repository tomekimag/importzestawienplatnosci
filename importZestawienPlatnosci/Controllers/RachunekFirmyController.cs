﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;

namespace importZestawienPlatnosci.Controllers
{
    public static class RachunekFirmyController
    {
        public static List<RachunekFirmyModel> PobierzRachunkiFirmy()
        {
            //MessageBox.Show($@"kwota = {kwota} email = {email}");

            List<RachunekFirmyModel> rachunki;

            using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                rachunki = (from rf in entity.RACHUNEK_FIRMY
                            where rf.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                            select new RachunekFirmyModel() {
                                AKTYWNY = rf.AKTYWNY,
                                ID_RACHUNKU = rf.ID_RACHUNKU,
                                KOD_RACHUNKU = rf.KOD_RACHUNKU,
                                KONTO_FK = rf.KONTO_FK,
                                LK = rf.LK,
                                NAZWA = rf.NAZWA,
                                NUMER_RACHUNKU = rf.NUMER_RACHUNKU,
                                SYM_WALUTY = rf.SYM_WALUTY
                            }).ToList();
            }

            return rachunki;
        }
    }
}
