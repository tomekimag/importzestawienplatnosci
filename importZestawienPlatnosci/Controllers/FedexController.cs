﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;
using LinqToExcel;
using System.Windows.Forms;
using System.Globalization;

namespace importZestawienPlatnosci.Controllers
{
    public static class FedexController
    {
        public static List<Fedex> pobierzPrzelewy(string sciezkaDoPliku)
        {
            try
            {
                List<Fedex> fedex;
                var excel = new ExcelQueryFactory(sciezkaDoPliku);


                excel.AddMapping<Fedex>(x => x.Data_przekazania_do_banku, "Data przekazania do banku");
                excel.AddMapping<Fedex>(x => x.Nr_listu, "Nr listu");
                excel.AddMapping<Fedex>(x => x.Data_nadania, "Data nadania");
                excel.AddMapping<Fedex>(x => x.Opis_zawartosci, "Opis zawartości");
                excel.AddMapping<Fedex>(x => x.Uwagi, "Uwagi");
                excel.AddMapping<Fedex>(x => x.Numer_zewnetrzny, "Numer zewnętrzny");
                excel.AddMapping<Fedex>(x => x.Odbiorca, "Odbiorca ");
                excel.AddMapping<Fedex>(x => x.Kwota, "Kwota");

                fedex = (from c in excel.WorksheetRange<Fedex>("A10", "H10000", "FedexPayOutReport")
                         where c.Nr_listu != string.Empty
                         select c).ToList();

                foreach (Fedex pozycja in fedex)
                {
                    pozycja.STATUS_POZYCJI = "Nie rozpoznany";
                    //MessageBox.Show($@"Data przekazania do banku = {pozycja.Data_przekazania_do_banku}");
                    pozycja.Data_przyjecia = DateTime.ParseExact(pozycja.Data_przekazania_do_banku, "‭yyyy-MM-dd‬", System.Globalization.CultureInfo.InvariantCulture);
                    pozycja.Data_wplaty = DateTime.ParseExact(pozycja.Data_nadania, "‭yyyy-MM-dd‬", System.Globalization.CultureInfo.InvariantCulture);
                    //pozycja.Data_przyjecia = DateTime.Parse(pozycja.Data_przekazania_do_banku.Replace('-','.'));
                    //pozycja.Data_wplaty = DateTime.Parse(pozycja.Data_nadania.Replace('-', '.'));
                }
                return fedex;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    MessageBox.Show($@"Błąd czytania pliku Fedex {ex.Message} {ex.InnerException.Message}");
                else
                    MessageBox.Show($@"Błąd czytania pliku Fedex {ex.Message}");
            }

            return null;
        }
    }
}
