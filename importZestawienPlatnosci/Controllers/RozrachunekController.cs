﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;
using System.Windows.Forms;
using importZestawienPlatnosci.Utils;

namespace importZestawienPlatnosci.Controllers
{
    public static class RozrachunekController
    {
        public static decimal PobierzIdRozrachunku(decimal kwota, string email)
        {
            //MessageBox.Show($@"kwota = {kwota} email = {email}");

            decimal idRozrachunku;

            using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                idRozrachunku = (from r in entity.ROZRACHUNEK
                            join dh in entity.DOKUMENT_HANDLOWY on r.ID_NAG_DOK_M equals dh.ID_DOKUMENTU_HANDLOWEGO
                            where (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                                              where (from pz in entity.POZYCJA_ZAMOWIENIA
                                                                     where (from z in entity.ZAMOWIENIE
                                                                            where z.WARTOSC_BRUTTO == kwota && (from k in entity.KONTRAHENT
                                                                                                                where k.ADRES_EMAIL == email
                                                                                                                select k.ID_KONTRAHENTA).Contains(z.ID_KONTRAHENTA??0)
                                                                            select z.ID_ZAMOWIENIA).Contains(pz.ID_ZAMOWIENIA??0)
                                                                     select pz.ID_POZYCJI_ZAMOWIENIA).Contains(pdm.ID_POZ_ZAM??0)
                                                              select pdm.ID_DOK_HANDLOWEGO).Contains(dh.ID_DOKUMENTU_HANDLOWEGO)
                            && r.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                            select r.ID_ROZRACHUNKU).FirstOrDefault();
            }

            return idRozrachunku;
        }


        public static decimal PobierzIdRozrachunkuFedex(string numer_przesylki)
        {
            //MessageBox.Show($@"kwota = {kwota} email = {email}");

            decimal idRozrachunku;

            using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                idRozrachunku = (from r in entity.ROZRACHUNEK
                                 join dh in entity.DOKUMENT_HANDLOWY on r.ID_NAG_DOK_M equals dh.ID_DOKUMENTU_HANDLOWEGO
                                 join pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO on dh.ID_DOKUMENTU_HANDLOWEGO equals pdm.ID_DOK_HANDLOWEGO
                                 join pz in entity.POZYCJA_ZAMOWIENIA on pdm.ID_POZ_ZAM equals pz.ID_POZYCJI_ZAMOWIENIA
                                 join z in entity.ZAMOWIENIE on pz.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                 where z.NUMER_PRZESYLKI == numer_przesylki
                                 && r.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                                 select r.ID_ROZRACHUNKU).FirstOrDefault();
            }

            return idRozrachunku;
        }

        public static List<RozrachunekModel> PobierzNierozliczoneRozrachunki()
        {
            //MessageBox.Show($@"kwota = {kwota} email = {email}");

            List<RozrachunekModel> rozrachunki = null;

            try
            {

                using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
                {
                    rozrachunki = (from r in entity.ROZRACHUNEK
                                   join dh in entity.DOKUMENT_HANDLOWY on r.ID_NAG_DOK_M equals dh.ID_DOKUMENTU_HANDLOWEGO
                                   where r.ID_NAG_DOK_M != 0 && r.TYP_DOK == "h" && r.POZOSTALO != 0 && r.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                                   select new RozrachunekModel()
                                   {
                                       KONTRAHENT_NAZWA = dh.KONTRAHENT_NAZWA,
                                       DATA_DOK = r.DATA_DOK,
                                       FORMA_PLATNOSCI = r.FORMA_PLATNOSCI,
                                       POZOSTALO_W = r.POZOSTALO_W,
                                       POZOSTALO = r.POZOSTALO,
                                       TYP_DOK = r.TYP_DOK,
                                       ID_ROZRACHUNKU = r.ID_ROZRACHUNKU,
                                       KIERUNEK_ZAPLATY = r.KIERUNEK_ZAPLATY,
                                       KWOTA = r.KWOTA,
                                       KWOTA_W = r.KWOTA_W,
                                       NAZWA_KONTA = r.NAZWA_KONTA,
                                       NR_DOK = r.NR_DOK,
                                       NR_KONTA = r.NR_KONTA,
                                       OPIS = r.OPIS,
                                       POZOSTALO_K = r.POZOSTALO_K,
                                       POZOSTALO_W_K = r.POZOSTALO_W_K,
                                       RODZAJ = r.RODZAJ,
                                       STRONA = r.STRONA,
                                       SYMBOL = r.SYMBOL,
                                       SYM_WALUTY = r.SYM_WALUTY,
                                       TERMIN_PLATNOSCI = r.TERMIN_PLATNOSCI,
                                       UWAGI = r.UWAGI
                                   }).ToList();
                }

                foreach (RozrachunekModel rozrachunek in rozrachunki)
                {
                    rozrachunek.DATA_DOK_ = DateHelper.InTtoDate(rozrachunek.DATA_DOK ?? 0);
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    MessageBox.Show($@"Błąd czytania rozrachunków: {ex.Message} {ex.InnerException.Message}");
                else
                    MessageBox.Show($@"Błąd czytania rozrachunków: {ex.Message}");
            }

            return rozrachunki;
        }

        public static decimal PobierzIdRozrachunkuPayPal(PayPal platnosc)
        {
            decimal idRozrachunku;

            using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                idRozrachunku = (from r in entity.ROZRACHUNEK
                                 join dh in entity.DOKUMENT_HANDLOWY on r.ID_NAG_DOK_M equals dh.ID_DOKUMENTU_HANDLOWEGO
                                 where (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                        where (from pz in entity.POZYCJA_ZAMOWIENIA
                                               where (from z in entity.ZAMOWIENIE
                                                      where z.WARTOSC_BRUTTO == platnosc.Brutto && (from k in entity.KONTRAHENT
                                                                                          where k.ADRES_EMAIL == platnosc.Z_adresu_email
                                                                                          select k.ID_KONTRAHENTA).Contains(z.ID_KONTRAHENTA ?? 0)
                                                      select z.ID_ZAMOWIENIA).Contains(pz.ID_ZAMOWIENIA ?? 0)
                                               select pz.ID_POZYCJI_ZAMOWIENIA).Contains(pdm.ID_POZ_ZAM ?? 0)
                                        select pdm.ID_DOK_HANDLOWEGO).Contains(dh.ID_DOKUMENTU_HANDLOWEGO)
                                 && r.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                                 select r.ID_ROZRACHUNKU).FirstOrDefault();
            }

            return idRozrachunku;
        }

        public static decimal PobierzIdRozrachunkuSchenker(Schenker platnosc)
        {
            // Przerobić metode w momencie gdy będę wiedział gdzie jest nr przesyłki na zamówieniu.
            decimal idRozrachunku = 0;
            /*
            using (AGROBIS_WFMAGEntities entity = new AGROBIS_WFMAGEntities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                idRozrachunku = (from r in entity.ROZRACHUNEK
                                 join dh in entity.DOKUMENT_HANDLOWY on r.ID_NAG_DOK_M equals dh.ID_DOKUMENTU_HANDLOWEGO
                                 where (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                        where (from pz in entity.POZYCJA_ZAMOWIENIA
                                               where (from z in entity.ZAMOWIENIE
                                                      where z.WARTOSC_BRUTTO == platnosc.Brutto && (from k in entity.KONTRAHENT
                                                                                                    where k.ADRES_EMAIL == platnosc.Z_adresu_email
                                                                                                    select k.ID_KONTRAHENTA).Contains(z.ID_KONTRAHENTA ?? 0)
                                                      select z.ID_ZAMOWIENIA).Contains(pz.ID_ZAMOWIENIA ?? 0)
                                               select pz.ID_POZYCJI_ZAMOWIENIA).Contains(pdm.ID_POZ_ZAM ?? 0)
                                        select pdm.ID_DOK_HANDLOWEGO).Contains(dh.ID_DOKUMENTU_HANDLOWEGO)
                                 && r.ID_FIRMY == ParametryUruchomienioweController.idFirmy
                                 select r.ID_ROZRACHUNKU).FirstOrDefault();
            }
            */
            return idRozrachunku;
        }
    }
}
