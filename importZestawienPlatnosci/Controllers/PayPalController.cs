﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;
using System.Windows.Forms;

namespace importZestawienPlatnosci.Controllers
{
    public static class PayPalController
    {
        public static List<PayPal> pobierzPrzelewy(string sciezkaDoPliku)
        {
            List<PayPal> listPayPal = new List<PayPal>();

            string sLinia;
            string[] separatingChar = { "\",\""};
            string[] values;
            bool bHeader = true;

            using (System.IO.StreamReader file = new System.IO.StreamReader(sciezkaDoPliku))
            {
                while (file.Peek() >= 0)
                {
                    sLinia = file.ReadLine();

                    if (bHeader) { bHeader = false; continue; }

                    values = sLinia.Split(separatingChar, System.StringSplitOptions.None);

                    //MessageBox.Show($"Length of line (count of fields) = {values.Count()}");
                    //MessageBox.Show($"Date value = {values[0].Substring(1)}");

                    PayPal payPal = new PayPal();
                    payPal.Data = DateTime.Parse(values[0].Substring(1), System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));

                    payPal.Godzina = values[1];

                    payPal.Strefa_czasowa = values[2];
                    payPal.Opis = values[3];
                    payPal.Waluta = values[4];

                    payPal.Brutto = decimal.Parse(values[5]);

                    //if (payPal.Brutto <= 0) { continue; }

                    payPal.Oplata = decimal.Parse(values[6]);
                    payPal.Netto = decimal.Parse(values[7]);
                    payPal.Saldo = decimal.Parse(values[8]);
                    payPal.Numer_transakcji = values[9];
                    payPal.Z_adresu_email = values[10];
                    payPal.Nazwa = values[11];
                    payPal.Nazwa_banku = values[12];
                    payPal.Rachunek_bankowy = values[13];
                    payPal.Koszt_wysylki_oraz_koszty_manipulacyjne = decimal.Parse(values[14]);
                    payPal.Kwota_podatku = decimal.Parse(values[15]);
                    payPal.Identyfikator_faktury = values[16];
                    //payPal.Pomocniczy_numer_transakcji = values[17].Substring(0, values[17].Lengthvalues[17].Length - 2);

                    listPayPal.Add(payPal);
                }
            }


            foreach (PayPal pozycja in listPayPal)
            {
                pozycja.STATUS_POZYCJI = "Nie rozpoznany";
            }

            return listPayPal;
        }
    }
}
