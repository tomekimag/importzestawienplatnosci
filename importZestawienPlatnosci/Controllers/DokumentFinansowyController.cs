﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlTypes;
using importZestawienPlatnosci.Model;
using importZestawienPlatnosci.Utils;

namespace importZestawienPlatnosci.Controllers
{
    public static class DokumentFinansowyController
    {
        public static void UtworzDokumentFinansowy(Przelew_24 pozycja)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ParametryUruchomienioweController.connectionStringToAdo))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo.MAGEXT_wystawBPdoPlatnosci", connection);

                    command.CommandType = CommandType.StoredProcedure;

                    command.CommandTimeout = 600;

                    SqlParameter id_firmy = command.Parameters.Add("@id_firmy", SqlDbType.Decimal);
                    id_firmy.Direction = ParameterDirection.Input;
                    id_firmy.Value = ParametryUruchomienioweController.idFirmy;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter id_uzytkownika = command.Parameters.Add("@id_uzytkownika", SqlDbType.Decimal);
                    id_uzytkownika.Direction = ParameterDirection.Input;
                    id_uzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter idRozrachunku = command.Parameters.Add("@idRozrachunku", SqlDbType.Decimal);
                    idRozrachunku.Direction = ParameterDirection.Input;
                    idRozrachunku.Value = pozycja.idRozrachunku;

                    SqlParameter kwota = command.Parameters.Add("@kwota", SqlDbType.Decimal);
                    kwota.Direction = ParameterDirection.Input;
                    kwota.Value = ((decimal)pozycja.Kwota_gr)/100;

                    SqlParameter data = command.Parameters.Add("@data", SqlDbType.Int);
                    data.Direction = ParameterDirection.Input;
                    data.Value = DateHelper.DateToInt(pozycja.Data_wplaty);

                    SqlParameter idKonta = command.Parameters.Add("@idKonta", SqlDbType.Decimal);
                    idKonta.Direction = ParameterDirection.Input;
                    idKonta.Value = pozycja.idRachunku;
                    
                    SqlParameter TYTUL_PRZELEWU = command.Parameters.Add("@TYTUL_PRZELEWU", SqlDbType.VarChar);
                    TYTUL_PRZELEWU.Direction = ParameterDirection.Input;
                    TYTUL_PRZELEWU.Value = pozycja.Tytul;
                    
                    var rdr = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static void UtworzDokumentFinansowy(Fedex pozycja)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ParametryUruchomienioweController.connectionStringToAdo))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo.MAGEXT_wystawBPdoPlatnosci", connection);

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 600;

                    SqlParameter id_firmy = command.Parameters.Add("@id_firmy", SqlDbType.Decimal);
                    id_firmy.Direction = ParameterDirection.Input;
                    id_firmy.Value = ParametryUruchomienioweController.idFirmy;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter id_uzytkownika = command.Parameters.Add("@id_uzytkownika", SqlDbType.Decimal);
                    id_uzytkownika.Direction = ParameterDirection.Input;
                    id_uzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter idRozrachunku = command.Parameters.Add("@idRozrachunku", SqlDbType.Decimal);
                    idRozrachunku.Direction = ParameterDirection.Input;
                    idRozrachunku.Value = pozycja.idRozrachunku;

                    MessageBox.Show($@"Kwota = {pozycja.Kwota}");

                    SqlParameter kwota = command.Parameters.Add("@kwota", SqlDbType.Decimal);
                    kwota.Direction = ParameterDirection.Input;
                    kwota.Value = pozycja.Kwota;

                    SqlParameter data = command.Parameters.Add("@data", SqlDbType.Int);
                    data.Direction = ParameterDirection.Input;
                    data.Value = DateHelper.DateToInt(pozycja.Data_wplaty);

                    SqlParameter idKonta = command.Parameters.Add("@idKonta", SqlDbType.Decimal);
                    idKonta.Direction = ParameterDirection.Input;
                    idKonta.Value = pozycja.idRachunku;

                    SqlParameter TYTUL_PRZELEWU = command.Parameters.Add("@TYTUL_PRZELEWU", SqlDbType.VarChar);
                    TYTUL_PRZELEWU.Direction = ParameterDirection.Input;
                    TYTUL_PRZELEWU.Value = $@"NrListu={pozycja.Nr_listu.Substring(2, pozycja.Nr_listu.Length - 2)}";

                    var rdr = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static void UtworzDokumentFinansowy(PayPal pozycja)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ParametryUruchomienioweController.connectionStringToAdo))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo.MAGEXT_wystawBPdoPlatnosci", connection);

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 600;

                    SqlParameter id_firmy = command.Parameters.Add("@id_firmy", SqlDbType.Decimal);
                    id_firmy.Direction = ParameterDirection.Input;
                    id_firmy.Value = ParametryUruchomienioweController.idFirmy;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter id_uzytkownika = command.Parameters.Add("@id_uzytkownika", SqlDbType.Decimal);
                    id_uzytkownika.Direction = ParameterDirection.Input;
                    id_uzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter idRozrachunku = command.Parameters.Add("@idRozrachunku", SqlDbType.Decimal);
                    idRozrachunku.Direction = ParameterDirection.Input;
                    idRozrachunku.Value = pozycja.idRozrachunku;

                    SqlParameter kwota = command.Parameters.Add("@kwota", SqlDbType.Decimal);
                    kwota.Direction = ParameterDirection.Input;
                    kwota.Value = (decimal)pozycja.Brutto;

                    SqlParameter data = command.Parameters.Add("@data", SqlDbType.Int);
                    data.Direction = ParameterDirection.Input;
                    data.Value = DateHelper.DateToInt(pozycja.Data);

                    SqlParameter idKonta = command.Parameters.Add("@idKonta", SqlDbType.Decimal);
                    idKonta.Direction = ParameterDirection.Input;
                    idKonta.Value = pozycja.idRachunku;

                    SqlParameter TYTUL_PRZELEWU = command.Parameters.Add("@TYTUL_PRZELEWU", SqlDbType.VarChar);
                    TYTUL_PRZELEWU.Direction = ParameterDirection.Input;
                    TYTUL_PRZELEWU.Value = pozycja.Numer_transakcji;

                    var rdr = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public static void UtworzDokumentFinansowy(Schenker pozycja)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ParametryUruchomienioweController.connectionStringToAdo))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("dbo.MAGEXT_wystawBPdoPlatnosci", connection);

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 600;

                    SqlParameter id_firmy = command.Parameters.Add("@id_firmy", SqlDbType.Decimal);
                    id_firmy.Direction = ParameterDirection.Input;
                    id_firmy.Value = ParametryUruchomienioweController.idFirmy;

                    SqlParameter id_magazynu = command.Parameters.Add("@id_magazynu", SqlDbType.Decimal);
                    id_magazynu.Direction = ParameterDirection.Input;
                    id_magazynu.Value = ParametryUruchomienioweController.idMagazynu;

                    SqlParameter id_uzytkownika = command.Parameters.Add("@id_uzytkownika", SqlDbType.Decimal);
                    id_uzytkownika.Direction = ParameterDirection.Input;
                    id_uzytkownika.Value = ParametryUruchomienioweController.idUzytkownika;

                    SqlParameter idRozrachunku = command.Parameters.Add("@idRozrachunku", SqlDbType.Decimal);
                    idRozrachunku.Direction = ParameterDirection.Input;
                    idRozrachunku.Value = pozycja.idRozrachunku;

                    SqlParameter kwota = command.Parameters.Add("@kwota", SqlDbType.Decimal);
                    kwota.Direction = ParameterDirection.Input;
                    kwota.Value = pozycja.Kwota;

                    SqlParameter data = command.Parameters.Add("@data", SqlDbType.Int);
                    data.Direction = ParameterDirection.Input;
                    data.Value = DateHelper.DateToInt(pozycja.Data_przelewu);

                    SqlParameter idKonta = command.Parameters.Add("@idKonta", SqlDbType.Decimal);
                    idKonta.Direction = ParameterDirection.Input;
                    idKonta.Value = pozycja.idRachunku;

                    SqlParameter TYTUL_PRZELEWU = command.Parameters.Add("@TYTUL_PRZELEWU", SqlDbType.VarChar);
                    TYTUL_PRZELEWU.Direction = ParameterDirection.Input;
                    TYTUL_PRZELEWU.Value = pozycja.Numer_listu;

                    var rdr = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
