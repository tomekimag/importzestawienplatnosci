﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importZestawienPlatnosci.Model;
using LinqToExcel;
using System.Windows.Forms;

namespace importZestawienPlatnosci.Controllers
{
    public static class SchenkerController
    {
        public static List<Schenker> pobierzPrzelewy(string sciezkaDoPliku)
        {
            List<Schenker> listaSchenker = new List<Schenker>();

            string sLinia;
            string[] separatingChar = { ";" };
            string[] values;
            bool bHeader = true;

            using (System.IO.StreamReader file = new System.IO.StreamReader(sciezkaDoPliku))
            {
                while (file.Peek() >= 0)
                {
                    sLinia = file.ReadLine();

                    values = sLinia.Split(separatingChar, System.StringSplitOptions.None);

                    if (values[1] == string.Empty) continue;

                    if (bHeader) { bHeader = false; continue; }

                    
                    

                    //MessageBox.Show($"Length of line (count of fields) = {values.Count()}");
                    //MessageBox.Show($"Date value = {values[0].Substring(1)}");

                    Schenker schenker = new Schenker();

                    schenker.Odbiorca = values[0];

                    schenker.Numer_listu  = values[1];
                    schenker.Opis = values[2];
                    schenker.Kwota = decimal.Parse(values[4]);

                    schenker.Data_przelewu = DateTime.Parse(values[5]);


                    listaSchenker.Add(schenker);
                }
            }


            foreach (Schenker pozycja in listaSchenker)
            {
                pozycja.STATUS_POZYCJI = "Nie rozpoznany";
            }

            return listaSchenker;
        }
    }
}
