--AGRO BIS
if exists (select 1 from sysobjects where name = 'MAGEXT_wystawBPdoPlatnosci' and type = 'P') 
   drop procedure dbo.MAGEXT_wystawBPdoPlatnosci
go
create procedure dbo.MAGEXT_wystawBPdoPlatnosci
     @id_firmy numeric,
     @id_magazynu numeric,
     @id_uzytkownika numeric,
     @idRozrachunku numeric,
	 @kwota decimal(10, 2),
	 @data int,
	 @idKonta numeric,
	 @TYTUL_PRZELEWU nvarchar(500)
as 
declare @errmsg varchar(255) 
  begin 
    set xact_abort on 
    set transaction isolation level REPEATABLE READ 
    begin transaction  
 
    --select @errmsg = 'Operacja dodatkowa Wystaw BP dla zaznaczonych rozrachunków nie została jeszcze zdefiniowana!'
    --goto Error 

	/*
	declare @Data Int
	declare @data_bez_czasu date
	set @data_bez_czasu = GETDATE()
	set @Data = cast(cast(@data_bez_czasu as datetime) as int) + 36163
	*/
	--select * from TYP_DOKUMENTU_FINANSOWEGO
	declare @ID_TYPU_DOKUMENTU_FINANSOWEGO [dbo].[T_ID_TYP]
	declare @czyUjemnaKwota tinyint = 0;

	if @kwota < 0
	begin
	--select * from TYP_DOKUMENTU_FINANSOWEGO
		set @kwota = @kwota * (-1) 
		
		select TOP 1 @ID_TYPU_DOKUMENTU_FINANSOWEGO = ID_TYPU 
		from TYP_DOKUMENTU_FINANSOWEGO 
		where ID_FIRMY = @id_firmy and SYGNATURA = 'BW'

		set @czyUjemnaKwota = 1;
	end
	else
	begin
		select TOP 1 @ID_TYPU_DOKUMENTU_FINANSOWEGO = ID_TYPU 
		from TYP_DOKUMENTU_FINANSOWEGO 
		where ID_FIRMY = @id_firmy and SYGNATURA = 'BP'
	end
	
	--select * from TYP_DOKUMENTU_FINANSOWEGO 

	declare @typ_platnika char(1)
	set @typ_platnika = 'k'

	declare @ID_PLATNIKA numeric

	declare @sym_wal Char(3)
	declare @KWOTA_WAL money 

	declare @ID_DOKUMENTU_FINANSOWEGO numeric

	
	declare @okres_numeracji tinyint

	declare @format_numeracji Varchar(50), @okresnumeracji Tinyint, @Parametr1 Tinyint, @Parametr2 TinyInt
	

	declare @ID_PRACOWNIKA numeric
	select top 1 @ID_PRACOWNIKA = id_pracownika from pracownik where ID_UZYTKOWNIKA = @id_uzytkownika


	select top 1 @ID_PLATNIKA = ID_KONTRAHENTA--, @sym_wal = SYM_WALUTY,
		--@KWOTA = POZOSTALO, @TYTUL_PRZELEWU = NR_DOK
	from ROZRACHUNEK where ID_ROZRACHUNKU = @idRozrachunku

	--if isNull(@sym_wal,'') not in ('', 'PLN') goto next_loop;

	set @KWOTA_WAL = 0.00

	exec AP_DodajDokumentFinansowy_Server @id_firmy, @ID_TYPU_DOKUMENTU_FINANSOWEGO, @ID_PLATNIKA, @typ_platnika, 
		@id_uzytkownika, @ID_DOKUMENTU_FINANSOWEGO output

		--exec AP_DodajPozycjeDlaDokFin @ID_DOKUMENTU_FINANSOWEGO, @KWOTA, 0, 0, @TYTUL_PRZELEWU_NIESTRUKTURALNY, 1, 'W', 'P'

	update ROZRACHUNEK set
		SEMAFOR = @id_uzytkownika,
		FLAGA_STANU = 3
	where id_rozrachunku = @idRozrachunku

	insert into ZAZNACZONE (ID_SESJI, UZYCIE, ID, ILOSC, ID2)
	select @id_uzytkownika, 5, @idRozrachunku, 0, 0

	exec AP_WciagnijRozrachunkiNaPozycje @ID_DOKUMENTU_FINANSOWEGO, @id_uzytkownika, ''

	update inter_fin_rozrach set kwota = @kwota where id_dokumentu_finansowego = @ID_DOKUMENTU_FINANSOWEGO and id_rozrachunku = @idRozrachunku

	exec JL_PobierzFormatNumeracji_Server @id_firmy, 3, @ID_TYPU_DOKUMENTU_FINANSOWEGO, 1/*id_kasy*/,
			@format_numeracji output, @okres_numeracji output, @Parametr1 output, @Parametr2 output

			--select * from KASY_FIRMY
	declare @id_rachunek_kasowy numeric
	set @id_rachunek_kasowy = @idKonta

	exec AP_ZatwierdzDokFin @Parametr1, @Parametr2, @ID_DOKUMENTU_FINANSOWEGO, @ID_TYPU_DOKUMENTU_FINANSOWEGO,
		@Data, @ID_PLATNIKA, @typ_platnika, @KWOTA, @id_firmy, @id_rachunek_kasowy, 
		@okres_numeracji, @format_numeracji, '', '', '', @TYTUL_PRZELEWU,
		@ID_PRACOWNIKA, '', '', @KWOTA_WAL, 1, '', 0

	delete from ZAZNACZONE where id = @idRozrachunku and ID_SESJI = @id_uzytkownika
	
    if @@trancount>0 commit transaction 
    goto Koniec 
Error: 

    raiserror (@errmsg,16,1) 
    if @@trancount>0 rollback tran 
    goto Koniec 
Koniec: 
    set transaction isolation level READ COMMITTED 
    return  
  end 
go
